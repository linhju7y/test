var sum = (array) => (array.length === 0 ? 0 : array[0] + sum(array.slice(1)));

var arr = [5, 9, 6, 8, 4, 6];

console.log("Sum of array using recursion: ", sum(arr));
