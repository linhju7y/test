function api(doCsl) {
  doCsl(1);
}

function api2(doCsl) {
  doCsl(2);
}

function api3(doCsl) {
  doCsl(3);
}

const main = async () => {
  await api(function (result) {
    console.log("result: ", result);
  });

  await api2(function (result2) {
    console.log("result2: ", result2);
  });

  await api3(function (result3) {
    console.log("result2: ", result3);
  });
};

main();
