function api(doCsl) {
  doCsl(1);
}

function api2(doCsl) {
  doCsl(2);
}

function api3(doCsl) {
  doCsl(3);
}

api(function (result) {
  console.log("result: ", result);
  api2(function (result2) {
    console.log("result2: ", result2);
    api3(function (result3) {
      console.log("result3: ", result3);
    });
  });
});
